/* eslint-disable indent */
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import Home from './components/Home'
import About from './components/About'
import Resources from './components/Resources'

Vue.use(VueRouter)
Vue.use(VueResource)

const routes = [
    { path: '/', component: Home },
    { path: '/about', component: About },
    { path: '/resources', component: Resources }
]

const router = new VueRouter({
    routes,
    mode: 'history'
})

new Vue({
  el: '#app',
  template: '<App/>',
  components: { App },
  router
}).$mount('#app')
